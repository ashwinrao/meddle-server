1 Directory Structure  
=====================

The contents of this folder are as follows

~~~
README                           - this file
Dockerfile.orig                  - the docker commands used to build the container 
module                           - script to create, remove, and instantiate the containers
fs                               - the folder which contains the folders to be placed in the "/" partition of the container
~~~ 
