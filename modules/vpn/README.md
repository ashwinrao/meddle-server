1 Directory Structure  
=====================

The contents of this folder are as follows

~~~
README                           - this file
Dockerfile.orig                  - the docker commands used to build the container 
Dockerfile.orig.strongswan-src   - the docker commands to build the container from the strongswan source. 
module                              - script to create, remove, and instantiate the containers
fs                               - the folder which contains the folders to be placed in the "/" partition of the container
~~~ 

2 Overview of vpn 
=================

The vpn meddle module spawn the strongswan ipsec daemon. 
The strongswan version used by default is the one provided by ubuntu. 
If this version is not suitable, then strongswan can also be compiled from the source.
The steps to create a container by compiling the strongswan source are available in the Dockerfile.orig.strongswan-src

The files in the fs/etc folder mounted in /etc of the container. 
The mapping between fs/etc and /etc in the container is controlled by the MOUNTPOINTS variable in the module script. 

The user credentials are present in the fs/etc/ipsec.secret file, while the certificates are present in the fs/etc/ipsec.d folder. 
The strongswan configuration commands are present in the fs/etc/strongswan.conf and fs/etc/strongswan.d folder. 


When a mobile device (vpn client) creates or terminates a vpn tunnel, strongswan invokes the /usr/lib/ipsec/\_updown script during the tunnel creation or termination process. 
This script in turn invokes /usr/lib/ipsec/\_signalupdown.
The \_signalupdown script publishes a mqtt  message to the mqtt broker running in the orchestrator.
The message is either of type MSG\_DEVICE\_UP or MSG\_DEVICE\_DOWN and the message contains 
- the timestamp
- the device identifier
- the private ip address assigned to the mobile device
- the public ip address of the mobile device
- the meddle server's IP address

The subscribers of this message type which include the packet monitor, and other meddle modules receive this message from this mqtt broker. 

3 Manually setting up the vpn module
=================================

To manually setup the container image execute the following commands.

~~~
set -a
source /replace/this/with/the/path/to/meddle.config > /dev/null 2>&1
./module setup
~~~

Note that the meddle.config must be loaded to ensure that the config variables are avaiable.
The ./module setup should create a container named meddle\_pktmonitor.
To check if the container has been created execute the following command

~~~
$ docker images
~~~

The output should be something like this

~~~~~
> docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
...
meddle_vpn            latest              81ca8599e142        3 weeks ago         283.7 MB
...
~~~~~

4 Manually spawning the vpn module to run in the foreground
========================================================

Once the container image has been created using the "module setup" command, the vpn module can be spawned as a background process or can be manually spawned for testing and debugging.
The vpn module internally assumes that the orchestrator is running. 
So before starting the vpn module, please ensure that the orchestrator is running!

Assuming that the orchestrator is running, and that you are in the modules/vpn folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module manualstart
~~~

This should create the container and give you a command prompt in the bash shell.
~~~
root@vpn:/#
~~~

Change to the /usr/sbin folder (or the path in which ipsec has been installed) 
~~~
root@vpn:/# /usr/sbin/ipsec status

root@vpn:/# /usr/sbin/ipsec start 
Starting strongSwan 5.1.2 IPsec [starter]...
ipsec\_starter[76]: Starting strongSwan 5.1.2 IPsec [starter]...
ipsec\_starter[89]: charon (90) started after 20 ms

root@vpn:/# /usr/sbin/ipsec status
Security Associations (0 up, 0 connecting):
  none
~~~

On another terminal you can see the tree of processes that created this container using the following command
~~~
ps -ef | grep "/usr/bin/docker -d" | awk '{print $2}' | head -n 1 | xargs pstree -ln
~~~


5 Manually spawning the vpn to run in the background
=====================================================

Once the container image has been created using the "module setup" command, the vpn module can be spawned as a background process or can be manually spawned for testing and debugging.
The vpn module internally assumes that the orchestrator is running.
So before starting the vpn module, please ensure that the orchestrator is running!

Assuming that the orchestrator is running, and that you are in the modules/vpn folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module start
~~~

You should see an indication of success.
In the same terminal n another terminal you can see the tree of processes that created this container using the following command
~~~
ps -ef | grep "/usr/bin/docker -d" | awk '{print $2}' | head -n 1 | xargs pstree -ln
~~~


6 Teleporting into a running module
=================================

It is possible to teleport inside a running module.
Assuming you are in the modules/orchestrator folder, execute the following command to spawn a bash shell inside a running module (container).

~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module teleport
~~~

This command is useful to perform tasks that are not possible from the host machine and require access to the container.



7 Manually stopping the vpn 
============================

Once the orchestrator has been created, it can be manually stopped.

Assuming you are in the modules/vpn folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module stop
~~~

Note that meddle server will appear to be down for the clients that use the vpn proxy


8 Misc
========

8.1 The directory structure of the fs folder is as follows. 
----------------------------------------------------------

The folders in the fs directory are as follows
~~~
fs/
├── etc
│   ├── ipsec.d                   -- contains the certificates aconfigurations
│   ├── ipsec.secrets             -- contains the credentials 
│   └── strongswan.d              -- contains the strongswan configurations
├── opt
│   └── vpn
│       ├── conf                  -- contains the meddle.config file
│       └── logs                  -- contains the logs generated by strongswan 
└── usr
    └── lib
        └── ipsec                 -- contains the scripts invoked when the a vpn tunnel is created and destroyed. 
~~~

The MOUNTPOINTS variable in the module script mounts the fs/opt/ into the /opt/ folder of the container.




8.2 Folder permissions 
----------------------

Make sure that folders can be traversed by root and docker
~~~
find fs  -type d -exec chmod 755 {} \;
~~~

Make sure that the config files are accessible and readable
~~~
find fs/ -type f -exec chmod 644 {} \;
~~~

Check if the binaries are executable
~~~
chmod 755 fs/usr/lib/ipsec.d/\* ;
~~~

8.3 Steps to update the fs folder to support a new version of strongswan
------------------------------------------------------------------------

Update the Dockerfile with the latest version of strongswan.

Create the container image
~~~
module setup 
~~~

Update the module mount points to a blank string to ensure that the fs folder is not mounted.
This can be achieved by setting MOUNTPOINTS="" in the module script.

If you wish to use the new config files present in the container, then copy the files using the following command. Note these will update any files present in the source tree.
~~~
docker cp vpn:/usr/lib/ipsec/\_updown fs/usr/lib/ipsec/
docker cp vpn:/etc/ipsec.d/ fs/etc/
docker cp vpn:/etc/ipsec.conf fs/etc/
docker cp vpn:/etc/ipsec.secrets fs/etc/
docker cp vpn:/etc/strongswan.conf fs/etc/
docker cp vpn:/etc/strongswan.d/ fs/etc/
~~~

Update the \_updown script and the other configuration files required. 

Push the changes to the repository.
