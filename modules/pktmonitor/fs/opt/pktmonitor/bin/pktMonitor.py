import mosquitto
import logging
import time
import meddleconf
import json 
import datetime
import subprocess
import os
import signal
import time


CLIENTNAME="pktMonitor"
MQTT_HOST=str(meddleconf.ORCHESTRATOR_HOSTNAME)
MQTT_PORT=str(meddleconf.ORCHESTRATOR_MESSAGEPORT)
MODULE_LOG="/opt/pktmonitor/logs/pktmonitor.log"
MODULE_LOGLEVEL=logging.DEBUG
SUBSCRIPTION=str(meddleconf.MSG_ROOT)+str("#")

class PktMonitor:
    mqttClient = None
    tcpdumpInstances = None 
    ipMap = None

    def __init__(self):
        """ Initialize """ 
        self.mqttClient = mosquitto.Mosquitto(CLIENTNAME)
        self.mqttClient.on_message   = self.processMessage
        self.mqttClient.on_connect   = self.processConnect        
        self.mqttClient.on_subscribe = self.processSubscribe        
        logging.basicConfig(filename=MODULE_LOG,level=MODULE_LOGLEVEL, format='%(asctime)s:%(levelname)s: %(message)s')
        logging.info("Done Initializing")
	self.tcpdumpInstances = dict()
	self.ipMap = dict()
        return
    
    def startBro(self):
        try:  
            #TODO:: Check for ethtap interface to come up here
            time.sleep(10); 
            for command in ["install", "check", "start"]:            
                cmdString = str(meddleconf.PKTMONITOR_BROCTL)+" " + str(command)
                logging.info(cmdString)
                tmp=cmdString.split()
                rc = subprocess.call(tmp, close_fds=True, stdout=None, stdin=None, stderr=None, shell=False)
                if rc != 0:
                   logging.error(str(cmdString)+" exited with a return code"+str(rc))
                   return False
        except Exception, e:
            logging.error("Unable to initiate bro :"+str(e))
            return False
        return True

    def stopBro(self):
        try:  
            for command in ["stop"]:
                cmdString = str(meddleconf.PKTMONITOR_BROCTL)+" " + str(command)
                logging.info(cmdString)
                tmp=cmdString.split()
                rc = subprocess.call(tmp, close_fds=True, stdout=None, stdin=None, stderr=None, shell=False)
                if rc != 0:
                   logging.error(str(cmdString)+" exited with a return code"+str(rc))
                   return False
        except Exception, e:
            logging.error("Unable to stop bro :"+str(e))
            return False
        return True
       

    """
    The actions to peform on connecting with the message broker
    """    
    def processConnect (self,obj, rc):
        logging.info("Connect to broker returned with code:"+str(rc))
        return        

    """
    Actions to perform when you have suscribed to messages managed by message broker
    """
    def processSubscribe(self, obj, mid, granted_qos):
        logging.info("Subscribed to message id "+str(mid)+" qos granted :"+str(granted_qos))
        return
        
    """
    Handling the various message. Add a function for each message type which must be processed by the messageHandler
    """
    def processMessage(self, obj, msg):
        logging.info("Received message: "+str(msg.topic)+" "+str(msg.qos)+" "+str(msg.payload))
        if msg.topic==meddleconf.MSG_DEVICE_UP:
            self.__processDeviceUp(msg.payload)
        elif msg.topic==meddleconf.MSG_DEVICE_DOWN:
            self.__processDeviceDown(msg.payload)
#        elif msg.topic==meddleconf.MSG_DEVICE_CONFCHANGE:
#           self.__processDeviceConfChange(msg.payload)
        else:
            logging.info("Message of Topic:"+str(msg.topic)+" is not handled")
        return True

    """
    Start tcpdump
    """
    def __startTcpdump(self, userName, privateIP, publicIP, meddleServer, jsonStr):
        """ Wrapper to the start the process """
        try:
            ts = datetime.datetime.utcnow()
            dateStamp=str(ts.day)+"-"+str(ts.month)+"-"+str(ts.year)
            timeStamp=str(ts.hour)+"-"+str(ts.minute)+"-"+str(ts.second)
            epochStamp=int(time.time())
            filename="tcpdump--"+str(dateStamp)+"--"+str(timeStamp)+"--"+str(userName)+"--"+str(privateIP)+"--"+str(publicIP)+"--"+str(meddleServer)+"--"+str(epochStamp)
            filepath=meddleconf.PKTMONITOR_PCAPPATH+"/"+str(userName)+"/"+str(dateStamp)+"/"
            if os.path.exists(filepath) is False:
                os.makedirs(filepath, mode=0o770)
            filename=str(filepath)+str(filename)
            cmdString = "/usr/sbin/tcpdump -s 0 -i "+str(meddleconf.PKTMONITOR_TAP_DEVICE)+" -w "+str(filename)+ " ip host "+str(privateIP)
            logging.info("Creating the process:"+str(cmdString))
            tmp=cmdString.split()
            logging.info(tmp)
            p = subprocess.Popen(tmp, close_fds=True, stdout=None, stdin=None, stderr=None, shell=False)
            self.tcpdumpInstances [userName] = {"json": str(jsonStr), "process":p} 
        except Exception, e:
            logging.error("Unable to initiate tcpdump for device :"+str(e))
            return False
        return True  


    """
    Stop tcpdump  
    """
    def __stopTcpdump(self, userName):
        """ Wrapper to the start the process """
        try:
            val = self.tcpdumpInstances.get(userName, None)
            p = None
            if val is not None:
                  p = val.get("process", None)
            if p is None:
               logging.error("Unable to find a tcpdump instance for device of userName:"+str(userName))
               return False  
            p.terminate()
            p.wait()
            self.tcpdumpInstances.pop(userName)
        except Exception, e:
            logging.error("Unable to terminate tcpdump for device :"+str(e))
            return False
        return True


    """
    Tasks to perform when the device comes up
    """
    def __processDeviceUp(self, msg):
        try:
            parsedMsg = json.loads(msg)
            userName = parsedMsg[meddleconf.MSG_FMT_DEVICE_USERNAME_KEY]
            devicePrivateIP = parsedMsg[meddleconf.MSG_FMT_DEVICE_PRIVIP_KEY]
            devicePublicIP = parsedMsg[meddleconf.MSG_FMT_DEVICE_PUBIP_KEY]
            meddleServer = parsedMsg[meddleconf.MSG_FMT_MEDDLE_SERVER_KEY]
            if False == self.__startTcpdump(userName, devicePrivateIP, devicePublicIP, meddleServer, msg):
               logging.error("Unable to initiate tcpdump for device of userName:"+str(userName)) 
            return
        except Exception, e:
            logging.error("Exception while processing device up command."+str(e))
        return	
        
    """
    Tasks to perform when the device goes down
    """
    def __processDeviceDown(self, msg):
        try:
            parsedMsg = json.loads(msg)
            userName = parsedMsg[meddleconf.MSG_FMT_DEVICE_USERNAME_KEY]
            if False == self.__stopTcpdump(userName):
                logging.error("stop tcpdump for device:"+str(userName)) 
            return
        except Exception, e:
            logging.error("Exception while processing device down command:"+str(e))  
        return 


    """
    Tasks to perform when the device configuration is changed
    """        
    def __processDeviceConfChange(self, msg):
        logging.info("Conf Hello")
        return

    """
    REPL
    """
    def waitLoop(self):        
        logging.info("Connecting to "+str(MQTT_HOST)+" on port "+str(MQTT_PORT))        
	self.mqttClient.connect(MQTT_HOST, int(MQTT_PORT), 60, True)
        logging.info("Subscribing to"+str(SUBSCRIPTION))        
	self.mqttClient.subscribe(SUBSCRIPTION,0)                 
        self.startBro()
        while self.mqttClient.loop() == 0:
            pass
        logging.info("Out of Loop"+str(self.mqttClient.loop()))
        return

    def handleExit(self):
        for k,v in self.tcpdumpInstances.items():
            logging.info("Terminating tcpdump instances for device"+str(k))
            # Use the key   
            val = self.tcpdumpInstances.get(k, None)
            p.terminate()
            p.wait() 
            logging.info("Terminated for device:"+str(k))
        logging.info("Terminated all processes")
        self.stopBro()
        return True


def sigTermHandler(signum, frame):
    """ Handle the TERM signal """
    m.handleExit()

if __name__ == "__main__":
   m = PktMonitor()
   signal.signal(signal.SIGTERM, sigTermHandler)
   m.waitLoop()            
