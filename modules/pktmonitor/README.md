1 Directory Structure  
=====================

The contents of this folder are as follows

~~~
README                           - this file
Dockerfile.orig                  - the docker commands used to build the container 
module                              - script to create, remove, and instantiate the containers
fs                               - the folder which contains the folders to be placed in the "/" partition of the container
~~~ 

Please check if the module is executable, the Dockerfile.orig is readable and fs folder can be traversed by the user root
~~~
chmod 755 module
chmod 644 Dockerfile.orig
chmod 755 fs
~~~

2 Overview of pktmonitor
========================

The pktmonitor spawn the bro ids and waits for the messages from the mqtt broker running in the orchestrator

On receiving a message which indicates a device is up, it spawns a tcpdump process to capture the packets exchanged.
This tcpdump process is terminated when a message indicating that the device is down is received. 

The packets captured using tcpdump are available in the fs/opt/pktmonitor/pcap-data/
The bro logs of the flows monitored using bro are available in the fs/opt/pktmonitor/bro-logs/ folder.


3 Manually setting up the pktmonitor module
========================================

To manually setup the container image execute the following commands.

~~~
set -a
source /replace/this/with/the/path/to/meddle.config > /dev/null 2>&1
./module setup
~~~

Note that the meddle.config must be loaded to ensure that the config variables are avaiable.
The ./module setup should create a container named meddle\_pktmonitor.
To check if the container has been created execute the following command

~~~
$ docker images
~~~

The output should be something like this

~~~~~
> docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
...
meddle\_pktmonitor     latest              8f5b2e7d4386        2 weeks ago         527.7 MB
...
~~~~~

4 Manually spawning the pktmonitor module to run in the foreground
==============================================================

Once the container image has been created using the "module setup" command, the vpn module can be spawned as a background process or can be manually spawned for testing and debugging.
The pktmonitor module internally assumes that the orchestrator is running. 
So before starting the pktmonitor module, please ensure that the orchestrator is running!

Assuming that the orchestrator is running, and that you are in the modules/vpn folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module manualstart
~~~

This should create the container and give you a command prompt in the bash shell.
~~~
root@pktmonitor:/#
~~~

Change to the /opt/pktmonitor/bin folder and start the pktmonitor  
~~~
root@pktmonitor:/# cd /opt/pktmonitor/bin/

root@pktmonitor:/opt/pktmonitor/bin# python pktMonitor.py 
warning: cannot read '/opt/pktmonitor/bro-logs/spool//broctl.dat' (this is ok on first run)
creating policy directories ... done.
installing site policies ... done.
generating standalone-layout.bro ... done.
generating local-networks.bro ... done.
generating broctl-config.bro ... done.
updating nodes ... done.
bro scripts are ok.
starting bro ...
...
...
~~~

On another terminal you can see the tree of processes that created this container using the following command
~~~
ps -ef | grep "/usr/bin/docker -d" | awk '{print $2}' | head -n 1 | xargs pstree -ln
~~~


5 Manually spawning the pktmonitor to run in the background
=====================================================

Once the container image has been created using the "module setup" command, the pktmonitor can be spawned as a background process or can be manually spawned for testing and debugging.
The pktmonitor internally assumes that the orchestrator is running.
So before starting the pktmonitor, please ensure that the orchestrator is running!

Assuming that the orchestrator is running, and that you are in the modules/vpn folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module start
~~~

You should see an indication of success.
In the same terminal n another terminal you can see the tree of processes that created this container using the following command
~~~
ps -ef | grep "/usr/bin/docker -d" | awk '{print $2}' | head -n 1 | xargs pstree -ln
~~~


6 Teleporting into a running module
=================================

It is possible to teleport inside a running module.
Assuming you are in the modules/pktmonitor folder, execute the following command to spawn a bash shell inside a running module (container).

~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module teleport
~~~

This command is useful to perform tasks that are not possible from the host machine and require access to the container.


7 Manually stopping the pktmonitor
==================================

Once the pktmonitor has been created, it can be manually stopped.

Assuming you are in the modules/pktmonitor folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module stop
~~~

Note that the packets exchanged in the vpn tunnels when the pktmonitor is not running are not captured. 


8 Misc
========

8.1 The directory structure of the fs folder is as follows. 
----------------------------------------------------------

The folders in the fs directory are as follows
~~~
fs/
└── opt
    └── pktmonitor             
        ├── bin              ... the executable python code for the packet monitor is here
        ├── bro              ... the bro scripts that have been modified are available in this folder
        │   └── etc          ... the bro config files are present in this folder
        ├── bro-logs         ... the folder which keeps the transient and persistent bro logs. 
        │   ├── logs         ... the persistent (encrypted) bro-logs are kept in this folder
        │   └── spool        ... the transient bro logs are placed in this folder
        ├── conf             ... the meddle.config file is in this folder
        ├── logs             ... the packet monitor logs are available in this folder
        └── pcap-data        ... the pcap files captured by tcpdump are available here. 
~~~

The MOUNTPOINTS variable in the module script mounts the fs/opt/ into the /opt/ folder of the container.
There are specific options for mounting the bro folder because we overwrite only some files in this container. 

8.2 Folder and file permissions 
-------------------------------

In the fs/opt/pktmonitor folder, make sure that root has write permissions to folders for pcap files and bro-logs.

~~~
find fs  -type d -exec chmod 755 {} \;
~~~

Make sure that the config files are accessible and readable
~~~
find fs/ -type f -exec chmod 644 {} \;
~~~

Check if the binaries are executable
~~~
find fs/opt/pktmonitor/bin -type f -exec chmod 755 {}  \;
~~~

The setup script performs these operations but it is better to check them to avoid surprises.


8.3 Modify the addresses in the fs/opt/bro/etc/networks.cfg
------------------------------------------------------------

If you are updating the network or the IP pool from which mobile devices are assigned an IP then please update
the addresses in the fs/opt/bro/etc/networks.cfg


8.4 Steps to update the fs folder to support a new version of bro
-----------------------------------------------------------------

Update the Dockerfile with the latest version of strongswan.

Create the container image
~~~
module setup 
~~~

Update the module mount points to a blank string to ensure that the fs folder is not mounted.
This can be achieved by setting MOUNTPOINTS="" in the module script.

If you wish to use the new config files present in the container, then copy the files using the following command. Note these will update any files present in the source tree.
~~~
docker cp pktmonitor:/opt/pktmonitor/bro fs/opt/pktmonitor/bro/ /
~~~

Update the confguration files and the mountpoint to include the configuration files you updates. You can remove the other files in the bro/etc/ folder 
Push the changes to the repository.

8.5 Installing bro from the .deb/rpm package
--------------------------------------------

I tried to install bro from the .deb pkg available in their site. However I received the following errors. 
~~~
bro : Depends: libc6 (< 2.12) but 2.19-0ubuntu6.5 is installed
      Depends: libpython2.6 (>= 2.6) but it is not installable
      Depends: libssl0.9.8 (>= 0.9.8m-1) but it is not installable
~~~
This is why we have to compile and install bro
