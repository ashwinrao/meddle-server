#!/bin/bash

echo "Setting up DB the /var/lib/mysql"
set -a 
alias docker=""
source /opt/dbserver/meddle.config > /dev/null 2>&1

/usr/bin/mysql_install_db > /dev/null 2>&1
if  [ $? -ne 0 ]
then
    echo "Error: Unable to setup DB"
    exit 1
fi

sync

(mysqld_safe > /dev/null 2>&1) &
if  [ $? -ne 0 ]
then
    echo "Error: Unable to fork mysqld"
    exit 1
fi

echo "Sleeping for 10 seconds for mysql to start for the first time"
sleep 10

echo "/usr/bin/mysqladmin -u root password \"${DBSERVER_ROOTPASSWORD}\""

/usr/bin/mysqladmin -u root password "${DBSERVER_ROOTPASSWORD}"
if  [ $? -ne 0 ]
then
    echo "Error: Unable to set the root password"
    exit 1
fi

mysql --user='root' --password="${DBSERVER_ROOTPASSWORD}" --execute "CREATE USER '${DBSERVER_MEDDLE_USERNAME}'@'%' IDENTIFIED BY '${DBSERVER_MEDDLE_PASSWORD}'"
if  [ $? -ne 0 ]
then
    echo "Error: Unable to create the meddle user"
    exit 1
fi

mysql --user='root' --password="${DBSERVER_ROOTPASSWORD}" --execute "CREATE DATABASE ${DBSERVER_MEDDLE_DBNAME}"
if  [ $? -ne 0 ]
then
    echo "Error: Unable to execute create the database"
    exit 1
fi

mysql --user='root' --password="${DBSERVER_ROOTPASSWORD}" --execute "GRANT ALL ON ${DBSERVER_MEDDLE_DBNAME}.* TO '${DBSERVER_MEDDLE_USERNAME}'@'%' WITH GRANT OPTION"
if  [ $? -ne 0 ]
then
    echo "Error: Unable to give access to MeddleDB"
    exit 1
fi

mysql --user="${DBSERVER_MEDDLE_USERNAME}" --password="${DBSERVER_MEDDLE_PASSWORD}" ${DBSERVER_MEDDLE_DBNAME} < /opt/dbserver/setup/CreateTables.sql
if  [ $? -ne 0 ]
then
    echo "Error: Unable to create the tables"
    exit 1
fi

#for sqlfile in `find /opt/dbserver/setup/ -name "*.sql"`;
#do 
#     if [ "${sqlfile}" != "/opt/dbserver/setup/CreateTables.sql" ]
#     then 
#         mysql --user="${DBSERVER_MEDDLE_USERNAME}" --password="${DBSERVER_MEDDLE_PASSWORD}" ${DBSERVER_MEDDLE_DBNAME} < ${sqlfile}
#         if  [ $? -ne 0 ]
#         then
#             echo "Error: Unable to install ${sqlfile}"
#             exit 1
#         fi
#     fi
#done
echo "Done"
