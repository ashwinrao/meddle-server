CREATE TABLE `UserAuthMap` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `authToken` varchar(128) NOT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `UserConfigChange` (
  `userID` int(11) NOT NULL,
  `timestamp` bigint(20) unsigned NOT NULL, 
  `configVariable` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newValue` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `UserConfigs` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `blockTrackers` int(11) NOT NULL DEFAULT '0',
  `monitorSSL` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `UserTunnelInfo` (
  `rowID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `clientTunnelIpAddress` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `clientRemoteIpAddress` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `serverIpAddress` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` bigint(20) unsigned NOT NULL,
  `startStopFlag` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rowID`),
  UNIQUE KEY `indexRowID` (`rowID`),
  KEY `indexTimeFlag` (`timestamp`,`startStopFlag`),
  KEY `indexTimestamp` (`timestamp`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
