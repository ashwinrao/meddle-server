The files in this folder are as follows as follows. 
~~~
setup
├── AppMetaData.sql             The mapping between the application name and the application ID
├── CDNAppMap.sql               The map between the CDNs and App. Some hostnames in CDNs are reserved for apps.
├── CreateTables.sql            The sql file to create the tables. 
├── PackageDetails.sql          The details of packages. This is generated from the google play data in misc-scripts and also manually inserting some app information.
├── README.md                   This file 
├── TrackerDomains.sql          The domains which are trackers.   
└── UserAgentSignatures.sql     The signatures extracted from the UserAgent. This is a cache for previously computed signatures. 
~~~
