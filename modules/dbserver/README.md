1 Directory Structure  
=====================

The contents of this folder are as follows

~~~
README                           - this file
Dockerfile.orig                  - the docker commands used to build the container 
module                           - script to create, remove, and instantiate the containers
fs                               - the folder which contains the folders to be placed in the "/" partition of the container
~~~ 

Please check if the module script is executable, the Dockerfile.orig is readable and fs folder can be traversed by the user root
~~~
chmod 755 module
chmod 644 Dockerfile.orig
chmod 755 fs
~~~

2 Overview of dbserver
========================

The dbserver runs mysql as the database server. This mysql instance is currently accessible only to processes and containers running on the host machine. 

3 Manually setting up the pktmonitor module
===========================================

To manually setup the container image execute the following commands.

~~~
set -a
source /replace/this/with/the/path/to/meddle.config > /dev/null 2>&1
./module setup
~~~

Note that the meddle.config must be loaded to ensure that the config variables are avaiable.
The ./module setup should create a container named meddle\_dbserver.
To check if the container has been created execute the following command

~~~
$ docker images
~~~

The output should be something like this

~~~~~
> docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
...
meddle\_dbserver      latest              8f5b2e7d4386        2 weeks ago         527.7 MB
...
~~~~~

4 Manually spawning the dbserver to run in the foreground
=========================================================

Once the container image has been created using the "module setup" command, the dbserver can be spawned as a background process or can be manually spawned for testing and debugging.
The dbserver module internally assumes that the orchestrator is running. 
So before starting the dbserver, please ensure that the orchestrator is running!

Assuming that the orchestrator is running, and that you are in the modules/dbserver folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module manualstart
~~~

This should create the container and give you a command prompt in the bash shell.
~~~
root@dbserver:/#
~~~

Now you need to start the mysql server
~~~
root@dbserver:/# mysqld\_safe
150420 14:06:07 mysqld\_safe Can't log to error log and syslog at the same time.  Remove all --log-error configuration options for --syslog to take effect.
150420 14:06:07 mysqld\_safe Logging to '/var/log/mysql/error.log'.
150420 14:06:07 mysqld\_safe Starting mysqld daemon with databases from /var/lib/mysql
...
...
~~~

On another terminal you can see the tree of processes that created this container using the following command
~~~
ps -ef | grep "/usr/bin/docker -d" | awk '{print $2}' | head -n 1 | xargs pstree -ln
~~~


5 Manually spawning the dbserver to run in the background
===========================================================

Once the container image has been created using the "module setup" command, the dbserver can be spawned as a background process or can be manually spawned for testing and debugging.
The dbserver internally assumes that the orchestrator is running.
So before starting the dbserver, please ensure that the orchestrator is running!

Assuming that the orchestrator is running, and that you are in the modules/dbserver folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module start
~~~

You should see an indication of success.
In the same terminal n another terminal you can see the tree of processes that created this container using the following command
~~~
ps -ef | grep "/usr/bin/docker -d" | awk '{print $2}' | head -n 1 | xargs pstree -ln
~~~


6 Teleporting into a running module
=================================

It is possible to teleport inside a running module.
Assuming you are in the modules/dbserver folder, execute the following command to spawn a bash shell inside a running module (container).

~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module teleport
~~~

This command is useful to perform tasks that are not possible from the host machine and require access to the container.


7 Manually stopping the dbserver
================================

Once the dbserver has been spawned, it can be manually stopped.

Assuming you are in the modules/dbserver folder, execute the following command to start the container.
~~~
set -a
source ../meddle.config > /dev/null 2>&1
./module stop
~~~

Note that the db state will be stale if the other meddle modules are running. 

8 Misc
========

8.1 The directory structure of the fs folder is as follows. 
----------------------------------------------------------

The folders in the fs directory are as follows
~~~

~~~

The MOUNTPOINTS variable in the module script mounts the folders in appropriate locations, for example, the fs/opt/ is mounted as the /opt/ folder of the container.
There are specific options for mounting the bro folder because we overwrite only some files in this container. 


8.2 Steps to update the fs folder to support a new version of mysql
-------------------------------------------------------------------

Update the Dockerfile with the latest version of strongswan.

Create the container image
~~~
./module setup 
~~~

Update the module mount points to a blank string to ensure that the fs folder is not mounted.
This can be achieved by setting MOUNTPOINTS="" in the module script.

If you wish to use the new config files present in the container, then copy the files using the following command. Note these will update any files present in the source tree.
~~~
docker cp dbserver:/etc/mysql/ fs/etc/mysql 
~~~

Update the confguration files and the mountpoint to include the configuration files you updates.

8.3 Taking a snapshot of database 
---------------------------------

Teleport into a running dbserver 

~~~
./module teleport
~~~

~~~
mysqldump
~~~

