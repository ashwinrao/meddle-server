import subprocess
import logging
import time
import signal
import sys
import datetime

# CONFIGURATION PARAMETERS FOR THE DNSSERVER USED BY THE MEDDLEAPPS
DNSMASQ_NAME="dnsmasq"
DNSMASQ_CMD="/usr/sbin/dnsmasq --conf-file=/opt/orchestrator/conf/dnsmasq.conf --no-daemon"
DNSMASQ_PIDFILE="/opt/orchestrator/conf/dnsmasq.pid"

# CONFIGURATION PARAMETERS FOR THE MQTT BROKER USED BY THE MEDDLEAPPS
MOSQUITTO_NAME="mosquitto"
MOSQUITTO_CMD="/usr/sbin/mosquitto -c /opt/orchestrator/conf/mosquitto.conf"

MESSAGEHANDLER_NAME="messageHandler"
MESSAGEHANDLER_CMD="python /opt/orchestrator/bin/messageHandler.py"

# CONFIGURATION PARAMETERS USED BY THIS MODULE
ORCHESTRATOR_LOG="/opt/orchestrator/logs/orchestrator.log"
ORCHESTRATOR_LOGLEVEL=logging.DEBUG
POLLING_INTERVAL=60


class Orchestrator:
    """ The main orchestrator for meddle. """
    __processes=None
  
    def __init__(self):        
        """ Initialize """ 
        self.__processes=dict()
        logging.basicConfig(filename=ORCHESTRATOR_LOG,level=ORCHESTRATOR_LOGLEVEL, format='%(asctime)s:%(levelname)s:%(message)s')

    def __startProcess(self, name, cmd):
        """ Wrapper to the start the process """
        try:
            logging.info("Creating the process:"+str(cmd))
            tmp=cmd.split()
            logging.info(tmp)
            p = subprocess.Popen(tmp)
            self.__processes[name]=p
            return True
        except Exception, e:
            logging.error("Unable to create the process:"+str(e))            
        return False

    def __writeDNSPid(self):      
        """ Write the PID file. This is useful to manually trigger some updates """
        try:
            logging.debug("Writing pid file")
            dnspid = self.__processes[DNSMASQ_NAME].pid
   	    f = open(DNSMASQ_PIDFILE, "w")
            f.write(str(dnspid))
            f.close()
            logging.info("Wrote "+str(DNSMASQ_PIDFILE))
        except Exception, e:
            logging.error("Error updating the pid file for "+str(DNSMASQ_NAME) + " "+str(e))
            return False
        return True
        
    def startProcesses(self):
        """ Start the child processes used by the orchestrator """
        logging.debug("Starting the processes")
        for n,c in [(DNSMASQ_NAME, DNSMASQ_CMD), (MOSQUITTO_NAME, MOSQUITTO_CMD), (MESSAGEHANDLER_NAME, MESSAGEHANDLER_CMD)]:
            if False == self.__startProcess(n,c):
                logging.error("Quitting on process creation error")
                return False         
        if False == self.__writeDNSPid():
           return False
        logging.debug("Created all processes")
        return True

    def waitLoop(self):
        """ Wait till you receive the kill signal. Log the status of the processes """
        flag=False
        logging.info("Starting the wait loop")
        while False==flag:
            time.sleep(POLLING_INTERVAL)
            logMsg="Health check at:"+str(datetime.datetime.now())+":"
            flag=False
            for k,v in self.__processes.items():
                try:
                    retcode = v.poll()  
                    if None == retcode:
                        logMsg = logMsg + str(k)+":OK:"
                    else:
                        flag=True                    
                        logMsg = logMsg + str(k)+":FAILED:Return code:"+str(retcode)+":"
                except Exception,e:
                    logging.error("Error in polling"+str(e))
            logging.info(logMsg)
        return True                                                              

    def handleExit(self):
        """ Handle the TERM signal """ 
        for k,v in self.__processes.items():
            logging.info("Terminating"+str(v))
            v.terminate()
            cnt=3
            while cnt > 0:
                if None == v.poll():
                    cnt=cnt-1;
                else:
                    break
            logging.info("Terminated"+str(k))
        logging.info("Terminated all processes") 
        return True            
            
    def mainLoop(self):
        if True == self.startProcesses():
           self.waitLoop()
           return True
        logging.error("Error while starting processes, quitting now")
        sys.exit(-1)

def sigTermHandler(signum, frame):
    """ Handle the TERM signal """ 
    o.handleExit()

if __name__=="__main__":
    o = Orchestrator()
    signal.signal(signal.SIGTERM, sigTermHandler)
    o.mainLoop()
