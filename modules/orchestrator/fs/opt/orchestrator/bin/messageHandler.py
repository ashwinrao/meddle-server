import mosquitto
import logging
import time
import meddleconf
import torndb
import socket
import json
import multiprocessing
import subprocess

CLIENTNAME="orchestratorMessageHandler"
MQTT_PORT=str(meddleconf.ORCHESTRATOR_MESSAGEPORT)
MODULE_LOG="/opt/orchestrator/logs/message_handler.log"
MODULE_LOGLEVEL=logging.DEBUG
SUBSCRIPTION=str(meddleconf.MSG_ROOT)+str("#")
DEVICE_UP=1
DEVICE_DOWN=2


class UserData:
    userID = None
    blockTrackers = None
    monitorSSL = None
    userName = None

    def __init__(self, userName, userID, blockTrackers, monitorSSL):
        self.userName = userName
        self.userID = userID
        self.blockTrackers = blockTrackers
        self.monitorSSL = monitorSSL

        

class MessageHandler:
    mqttClient = None
    dbConn = None
    dataLock = None
    userMap = None
    portMap = None
    meddleServerIP = None
    meddleTrackerFilterIP = None
    SSLPorts = None

    def __init__(self):
        """ Initialize """
        self.mqttClient = mosquitto.Mosquitto(CLIENTNAME)
        self.mqttClient.on_message   = self.processMessage
        self.mqttClient.on_connect   = self.processConnect
        self.mqttClient.on_subscribe = self.processSubscribe
        logging.basicConfig(filename=MODULE_LOG,level=MODULE_LOGLEVEL, format='%(asctime)s:%(levelname)s: %(message)s')

        self.dbConn = None
        self.dataLock = multiprocessing.Lock()
        self.userDataCache = dict()
        self.portMap = dict()
        self.meddleServerIP = None
        self.meddleTrackerFilterIP = None
        self.SSLPorts = meddleconf.SSLMONITOR_SSLPORTS.split(",")
        logging.info("Done Initializing")
        return

    """
    The actions to peform on connecting with the message broker
    """
    def processConnect (self,obj, rc):
        logging.info("Connected return code:"+str(rc))
        return

    """
    Actions to perform when you have suscribed to messages managed by message broker
    """
    def processSubscribe(self, obj, mid, granted_qos):
        logging.info("Subscribed to "+str(mid) + str(granted_qos))
        return

    """
    Handling the various message. Add a function for each message type which must be processed by the messageHandler
    """
    def processMessage(self, obj, msg):
        logging.info("Received message:"+str(msg.topic)+" "+str(msg.qos)+" "+str(msg.payload))
        if msg.topic==meddleconf.MSG_DEVICE_UP:
            self.__processDeviceUpDown(msg, DEVICE_UP)
        elif msg.topic==meddleconf.MSG_DEVICE_DOWN:
            self.__processDeviceUpDown(msg, DEVICE_DOWN)
        elif msg.topic==meddleconf.MSG_DEVICE_CONFCHANGE:
            self.__processDeviceConfChange(msg)
        else:
            logging.info("Message of Topic:"+str(msg.topic)+" is not handled")
        #endif
        return
            
    """
    the portMap contains the dictionary {"vethads":4, ...}
    the portMap is required for modifying the forwarding tables of the switch 
    """
    def __populateSwitchPortMap(self):
        """        
        Currently using the ovs-command. This can be updated using the REST API
        of a SDN controller such as ryu and ONOS at a later date
        """
        try:
            self.meddleServerIP = socket.gethostbyname(str(meddleconf.MEDDLE_HOSTNAME))            
            if self.meddleServerIP == "":
                logging.error("Unable to get the IP the Meddle Server")
                return False

            self.meddleTrackerFilterIP = socket.gethostbyname(meddleconf.TRACKERFILTER_HOSTNAME)
            if self.meddleTrackerFilterIP == "":
                logging.error("Unable to get the IP the Meddle Server")
                return False
                
            ovs_cmd  = "ovs-ofctl dump-ports-desc tcp:"+str(self.meddleServerIP)
            logging.debug("Executing the command"+str(ovs_cmd))
                
            # NOTE:: UPDATE THIS LIST WHEN A NEW MEDDLE APP IS CREATED!
            interface_list=[meddleconf.ORCHESTRATOR_VETH_DEVICE, meddleconf.PKTMONITOR_VETH_DEVICE,
                            meddleconf.VPN_VETH_DEVICE, meddleconf.DBSERVER_VETH_DEVICE,
                            meddleconf.TRACKERFILTER_VETH_DEVICE, meddleconf.SSLMONITOR_VETH_DEVICE]
            logging.debug(interface_list)
            try_cnt = 10
            while try_cnt >= 0:
                try_cnt = try_cnt - 1                
                # tries required for the containers to be fired

                ovs_proc = subprocess.Popen(ovs_cmd.split(), stdout=subprocess.PIPE)
                proc_resp = ovs_proc.stdout.read()
                if proc_resp is None or proc_resp == "":
                    logging.error("Error executing the command:"+str(ovs_cmd))
                    return False
                #endif
                
                for line in proc_resp.split("\n"):
                    for interface in interface_list:
                        if line.find(interface) !=-1:
                            lst = line.split("("+str(interface)+")")
                            if len(lst) > 1:
                                self.portMap[interface] = str(int(lst[0]))
                            #endif
                        #endif
                    #endfor
                #endfor

                logging.debug("attempt:"+str(try_cnt)+" PortMap:"+str(self.portMap))                
                if len(self.portMap.keys())!= len(interface_list):
                    logging.error("Unable to find the port for all list:found:"+str(self.portMap.keys())+":reqd:"+str(interface_list))
                else:
                    logging.info("Found port for all interfaces")
                    break
                #end if
            # end while        
            # Now check if we have the interfaces        
            if len(self.portMap.keys())!= len(interface_list):
                logging.error("Unable to find the port for all list:found:"+str(self.portMap.keys())+":reqd:"+str(interface_list))
                return False
            #endif                
        except Exception,e:
            logging.error("Exception while finding port Map "+str(e))
            return False
        #end try
        logging.info("Found the list of ports from the switch:" + str(self.portMap))
        return True
        

    def __fetchUserData(self, userName):
        userID = None
        query = "SELECT * from UserConfigs WHERE userName='"+str(userName)+"';"
        self.dataLock.acquire()        
        try:
            results = self.dbConn.query(query)
            if results is not None and len(results) == 1:
                resultRow = results[0]
                userData = UserData(userName, int(resultRow.userID), int(resultRow.blockTrackers), int(resultRow.monitorSSL))
                self.userDataCache[str(userName)] = userData
            else:
                logging.error("Unable to fetch a unique userID for userName:"+str(userName)+": query result is "+str(results))
            #endif
        except Exception,e:
            logging.error("Error executing query:"+str(query)+": exception:"+str(e))
        #end try
        self.dataLock.release()
        return userData

    def __getUserData(self, userName):
        self.dataLock.acquire()
        userData = self.userDataCache.get(userName, None)
        self.dataLock.release()
        if None == userData:
            userData = self.__fetchUserData(userName)
            if None == userData:
                logging.error("Unable to fetch the userData for "+str(userName))
                return None
            #endif
        #endif
        return userData

    def __updateDBEntry(self, userData, privIP, pubIP, server, timestamp, devState):
        userID = userData.userID
        query = "INSERT INTO UserTunnelInfo VALUES (0,'"+str(userID)+"','"+str(privIP)+"','"+str(pubIP)+"','"+str(server)+"','"+str(timestamp)+"','"+str(devState)+"');"
        ret=True
        logging.debug("Executing query:"+str(query))
        self.dataLock.acquire()
        try:
            results = self.dbConn.execute(query)
            if results is None or results == 0:
                logging.error("Error updating the DB")
                ret=False
            #endif
        except Exception, e:
            logging.error("Exception executing query:"+str(query)+":exception:"+str(e))
        #end try
        self.dataLock.release()
        return ret

    def __addDNSRespRule(self):
        try:
            vpn_port = self.portMap.get(str(meddleconf.VPN_VETH_DEVICE), None)
            defaultDNS = meddleconf.MEDDLE_DNSIP
            # All DNS responses coming from the adblocking server should look like they are coming from the original dns server 
            ovs_cmd = "ovs-ofctl add-flow tcp:"+str(self.meddleServerIP)+" priority=2,udp,udp_src=53,nw_src="+str(self.meddleTrackerFilterIP)+",actions=mod_nw_src:"+str(defaultDNS)+",output="+str(vpn_port)
            if 0 != subprocess.call(ovs_cmd.split()):
                logging.error("Error invoking command"+str(ovs_cmd))
                return False
            # TODO:: Better error handling
            logging.info("Success in executing :"+str(ovs_cmd))            
        except Exception, e:
            logging.error("Exception when populating the response"+str(e))
            return False
        return True  
        

    def __updateFlowTables(self, userData, privIP, devState):
        # TODO:: check here is the user has enabled blocking based on the userID
        logging.info("Updating the flow tables for"+str(userData.userID))
        ovs_cmds = []
        try:
            dns_port = self.portMap.get(str(meddleconf.TRACKERFILTER_VETH_DEVICE), None)
            sslmon_port = self.portMap.get(str(meddleconf.SSLMONITOR_VETH_DEVICE), None)
            if dns_port == None or sslmon_port == None:
                logging.error("Unable to get the port for "+str(meddleconf.TRACKERFILTER_VETH_DEVICE) + " or " + str(meddleconf.SSLMONITOR_VETH_DEVICE) + " Port map:"+str(self.portMap))
                return False                
            if devState == DEVICE_UP:
                ### For tracking
                if userData.blockTrackers == 1:                    
                    ovs_cmds.append("ovs-ofctl add-flow tcp:"+str(self.meddleServerIP)+" priority=2,udp,udp_dst=53,nw_src="+str(privIP)+",actions=mod_nw_dst:"+str(self.meddleTrackerFilterIP)+",mod_dl_dst="+str(meddleconf.TRACKERFILTER_MAC_ADDRESS)+",output="+str(dns_port))
                else:
                    logging.debug("User '"+str(userData.userName)+"' does not want to block trackers")

                ## For Monitoring SSL
                if userData.monitorSSL == 1:
                    for port in self.SSLPorts:
                        ovs_cmds.append("ovs-ofctl add-flow tcp:"+str(self.meddleServerIP)+" priority=2,tcp,tcp_dst="+str(port)+",nw_src="+str(privIP)+",actions=mod_dl_dst="+str(meddleconf.SSLMONITOR_MAC_ADDRESS)+",output="+str(sslmon_port))
            elif devState == DEVICE_DOWN:
                if userData.monitorSSL == 1:
                    for port in self.SSLPorts:
                        ovs_cmds.append("ovs-ofctl del-flows --strict tcp:"+str(self.meddleServerIP)+" priority=2,tcp,tcp_dst="+str(port)+",nw_src="+str(privIP))
                if userData.blockTrackers == 1:                    
                    ovs_cmds.append("ovs-ofctl del-flows --strict tcp:"+str(self.meddleServerIP)+" priority=2,udp,udp_dst=53,nw_src="+str(privIP))
            else:
                logging.error("The current state is not supported!")
                return False
            #endif
            for ovsc in ovs_cmds:
                if 0 != subprocess.call(ovsc.split()):
                    logging.error("Error invoking command"+str(ovsc))
                    return False
                    # TODO:: Better error handling    
                logging.info("Success in executing :"+str(ovsc))                    
        except Exception, e:
            logging.error("Exception"+str(e))
            return False                          
        return True
            
    """
    Tasks to perform when the device state changes 
    1. Add the entry to the database
    2. Update forwarding rules
    """
    def __processDeviceUpDown(self, msg, device_state): 
        logging.info("Processing Device Up/Down")        
        # Parsing the message
        parsedMsg = json.loads(msg.payload)
        userName = parsedMsg[meddleconf.MSG_FMT_DEVICE_USERNAME_KEY]
        devicePrivateIP = parsedMsg[meddleconf.MSG_FMT_DEVICE_PRIVIP_KEY]
        devicePublicIP = parsedMsg[meddleconf.MSG_FMT_DEVICE_PUBIP_KEY]
        meddleServer = parsedMsg[meddleconf.MSG_FMT_MEDDLE_SERVER_KEY]
        timestamp = parsedMsg[meddleconf.MSG_FMT_TIMESTAMP_KEY]

        # Fetching Database keys
        userData = self.__getUserData(userName)
        if None == userData:
            logging.error("Unable to process the message"+str(msg.payload))
            return

        # Updating the DB entry
        if False == self.__updateDBEntry(userData, devicePrivateIP, devicePublicIP, meddleServer, timestamp, device_state):
            logging.error("Error in updating the DB Entry for Tunnels")

        if False == self.__updateFlowTables(userData, devicePrivateIP, device_state):
            logging.error("Error updating the flow table Entry")
            
        logging.info("Success in processing message:"+str(msg.payload))    
        return
        
    """
    Tasks to perform when the device configuration is changed
    """
    def __processDeviceConfChange(self, msg):
        logging.info("Processing configuration Change")
        return

    def __connectDB(self):
        ipAddr = None
        cnt = 10
        """ Wait for the dbserver to come up """
        while ipAddr is None and cnt > 0:
            cnt = cnt - 1
            try:
                ipAddr = socket.gethostbyname(str(meddleconf.DBSERVER_HOSTNAME))
                cnt = 0
            except:
                logging.info("Waiting for the "+str(meddleconf.DBSERVER_HOSTNAME)+" to come up, attempts remaining:"+str(cnt))
                ipAddr = None
                time.sleep(2)
            #end try
        #end while
        if ipAddr is None:
            logging.error("Unable to resolve the IP address of "+str(meddleconf.DBSERVER_HOSTNAME))
            return False
        #endif 

        """ Connect to the database """
        cnt = 3
        while cnt >= 0:
            cnt = cnt - 1
            try:
                self.dbConn = torndb.Connection(user=meddleconf.DBSERVER_MEDDLE_USERNAME, password=meddleconf.DBSERVER_MEDDLE_PASSWORD, database=meddleconf.DBSERVER_MEDDLE_DBNAME,host=meddleconf.DBSERVER_HOSTNAME)
                break
            except:
                logging.error("Unable to connect to the database:"+str(meddleconf.DBSERVER_HOSTNAME)+" attempts remaining:"+str(cnt))
            #end try
        #end while

        if 0 > cnt:
            logging.error("Failure in connecting to the database"+str(meddleconf.DBSERVER_HOSTNAME))
            return False
        #endif 
        logging.info("Success in connecting to "+str(meddleconf.DBSERVER_HOSTNAME))    
        return True
        
    """
    REPL
    """
    def waitLoop(self):
        logging.info("Connecting to "+str(meddleconf.ORCHESTRATOR_HOSTNAME)+" on port "+str(MQTT_PORT))
	self.mqttClient.connect(str(meddleconf.ORCHESTRATOR_HOSTNAME), int(MQTT_PORT), 60, True)
        logging.info("Subscribing to"+str(SUBSCRIPTION))
	self.mqttClient.subscribe(SUBSCRIPTION,0)
        if False == self.__connectDB():
            logging.error("Message handler exiting due to DB error")
            return
        if False == self.__populateSwitchPortMap():
            logging.error("Unable to get the ports for the containers")
            return
        if False == self.__addDNSRespRule():
            logging.error("Unable to add the common DNS Response Rule")
            return
            
        while self.mqttClient.loop() == 0:
           pass
        logging.info("Out of Loop for receiving messages"+str(self.mqttClient.loop()))
        return

if __name__ == "__main__":
   m = MessageHandler()
   m.waitLoop()
