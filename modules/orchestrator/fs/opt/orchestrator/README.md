Directory Structure
===================

The directory structure of the /opt/orchestrator/ folder of the container is as follows.
~~~
/opt/orchestrator/
├── bin
│   ├── meddleconf.py        The link the meddle.config folder in conf
│   ├── messageHandler.py    The main message handler. 
│   ├── orchestrator.py      The orchestrator which spawns a) dnsmasq server, b) the mosquitto broker, and c) the message server
│   └── signalnewhosts       The script which is executed to signal that the hosts file contains the IP addresses of the containers.  
├── conf                     The folder which contains the configuration files used by the daemons spawned by the orchestrator.  
│   ├── dnsmasq.conf         The configuration file for dnsmasq. This file explicitly mentions that the hosts file in this folder has the mapping between hostname and IPs for the containers. 
│   ├── dnsmasq.conf.orig    The template from which the dnsmasq.conf was created.
│   ├── hosts                The hosts file which is updated when the containers are spawned. 
│   ├── meddle.config        The master meddle.config file. The file in the meddleapps folder is copied to this location. 
│   ├── mosquitto.conf       The configuration for the mosquitto broker. 
│   └── mosquitto.db         The database used by the mosquitto broker   
├── logs                     The folder that contains the dnsmasq and mosquitto logs. 
└── README                   This file
~~~

Checks
======

# Make sure the the owner of logs is root
~~~
chown -R root.root logs
~~~


# For the bin and conf folders make sure that the others have read and execute permissions 
~~~
chmod 755 bin
chmod 755 conf
~~~
