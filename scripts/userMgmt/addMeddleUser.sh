set -a
source ./meddle.config > /dev/null 2>&1

function usage {
    echo "Usage: $0 [options <value>]"
    echo "Options"
    echo " -a, --auto      automatically generate username and password"
    echo " -u, --username  <username>"
    echo " -p, --password  <password>"
    echo " -h, --help      this help"
}


# Parse the arguments
function parse_args {
    #-u for removing quotes if any, options with arguments have a ':'
    if ! options=$(getopt -u -o  ahu:p: -l help,auto,username:,password: -- "$@") ;
    then
        usage $0        
        exit 1
    fi 
    
    set -- ${options}

   while [ $# -gt 0 ]
   do
       case $1 in
           -u|--username) clientName="${2}"; shift ;;
           -p|--password) clientPassword="${2}"; shift ;;
           -a|--auto)     autoCred=1; shift ;; 
           -h|--help)     usage; exit 0;;
           (--)           shift; break;;
           (-*)           echo "$0: error - unrecognized option $1" 1>&2; usage $0 ; exit 1;;
           (*)            break;;
       esac
       shift
   done
}

function performChecks {
    # First check if the required softwares are available
    if [ "`which ipsec`" == "" ] 
    then
        echo "ERROR: Unable to locate the ipsec binary of strongswan."
        echo "Please install strongswan using the following command on ubuntu/debian systems: sudo apt-get install strongswan"
        return 1
    fi 

    if [ "`which openssl`" == "" ] 
    then
        echo "ERROR: Unable to locate openssl"
        echo "Please install openssl"
        return 1
    fi 

    if [ "`which mysql`" == "" ] 
    then
        echo "ERROR: Unable to locate mysql"
        echo "Please install mysql"
        return 1
    fi 


    # This is a hacky check to see if the DB Server and the VPN server are running
    if [ "${DBSERVER_IP}" == "" ] || [ "${VPN_IP}" == "" ]
    then
        echo "ERROR: Meddle must be running!"
        return 1
    fi
    return 0
}

function createCredentials {
    tmpID=`date +%s | sha1sum | cut -b -10`
    tmpPass=`echo ${tmpID} | cut -b -5`
    
    clientName="$tmpID"
    clientPassword="$tmpPass"
    return 0
}

function setVariables {
    clientCertPath="${PWD}/clientCertificates/${clientName}/"
    caCertPath="${PWD}/caCertificates/"

    # The certificate files
    clientKeyFile="${clientCertPath}/${clientName}-Key.pem"
    clientCertFile="${clientCertPath}/${clientName}-Cert.pem"
    clientP12File="${clientCertPath}/${clientName}.p12" 

    # Certifying authority files
    caCert="${caCertPath}/${VPN_CA_CERTIFICATE}"
    caKey="${caCertPath}/${VPN_CA_PRIVKEY}"
    caName="${VPN_CA_NAME}"

    # Some strings in the certificates
    dnStr="C=${VPN_CA_COUNTRY}, O=${VPN_CA_ORGANIZATION}, CN=${clientName}"
    orgName=${VPN_CA_ORGANIZATION}
    displayName=${VPN_CA_ORGANIZATION}
    serverHostName=${MEDDLE_HOSTNAME}
}

function createP12File {     
    # Create the path to store client certificates
    mkdir -p ${clientCertPath} > /dev/null 2>&1

    # Create the key file
    #echo "Generating the private key file: ${clientKeyFile}"
    ipsec pki --gen --outform pem > ${clientKeyFile} 2>/dev/null
    openssl rsa -in ${clientKeyFile} -check -noout > /dev/null 2>&1
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to create the key file ${clientKeyFile} for the certificate"
        return 1
    fi
    #echo "Success in generating the private key file: ${clientKeyFile}"

    # Create the CA signed certificate using the key file and sign it using the CA certificate"
    #echo "Generating the certificate file: ${clientCertFile}"
    cmd="ipsec  pki --pub --in ${clientKeyFile} | ipsec pki --issue --cacert ${caCert} --cakey ${caKey} --dn \"${dnStr}\" --outform pem > ${clientCertFile}"
    #echo ${cmd}
    # Stupid bash bug made me use eval to get the quotes for DN working :(.
    eval ${cmd} 
    openssl x509 -in "${clientCertFile}" -noout > /dev/null 2>&1
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to create a signed certificate"
        return 1
    fi
    #echo "Success in creating certificate: ${clientCertFile}"

    # Create the .p12 file
    # Example: openssl pkcs12 -export -inkey /tmp/abcKey6906.pem -in /tmp/abcCert6906.pem -name "client" -certfile /home/arao/etc/ipsec.d/cacerts/caCert.pem -caname "snowmane CA" -out abc.p12 -passout pass:hello

    #echo "Generating the .p12 file:${clientP12File}"
    openssl pkcs12 -export -inkey "${clientKeyFile}" -in "${clientCertFile}" -name "${clientName}" -passout pass:"${clientPassword}" -certfile "${caCert}" -caname \""${caName}"\" -out "${clientP12File}"
    if [ $? -ne 0 ] || [ ! -f ${clientP12File} ];
    then
        echo "ERROR: Unable to create a signed certificate"
        return 1
    fi
    #echo "Success in creating certificate: ${clientP12File}"
    return 0
}

function generateMobileConfig {    
#python genIOSConfigXML.py "${clientName}" "${clientPassword}" "${mobileConfigOrgName}" "${mobileConfigConDisplayName}" "${caName}" "${mobileConfigServerHostname}" "${signingCertsPath}${caCertName}" "${clientCertsPath}" 
    python genIOS7ConfigXML.py "${clientName}" "${clientPassword}" "${orgName}" "${displayName}" "${caName}" "${serverHostname}" "${caCert}" "${clientCertPath}" > /dev/null 2>&1   
    if [ $? -ne  0 ];
    then
        echo "ERROR: Unable to create the .mobileconfig file"
        return 1
    fi
    #echo "Success in creating the .mobileconfig file"
    return 0        
}


function createDBEntries {
    # Insert an entry in UserConfigs for ${clientName}
    query="INSERT INTO UserConfigs VALUES (0, '${clientName}', '${MEDDLE_USERDEFAULTS_BLOCKTRACKERS}', '${MEDDLE_USERDEFAULTS_MONITORSSL}');"
    mysql -u "${DBSERVER_MEDDLE_USERNAME}" --password="${DBSERVER_MEDDLE_PASSWORD}" -D "${DBSERVER_MEDDLE_DBNAME}" -h "${DBSERVER_IP}" -e "${query}"
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to create an entry for ${clientName} in UserConfigs Table"
        return 1
    fi

    # Insert and entry in UserAuthMap for the authentication code
    authCode=`date +%s``uptime``free`${clientName}
    authCode=`echo ${authCode}| md5sum | cut -d ' ' -f 1`
    query="insert Into UserAuthMap VALUES (0, '${authCode}');"
    mysql -u "${DBSERVER_MEDDLE_USERNAME}" --password="${DBSERVER_MEDDLE_PASSWORD}" -D "${DBSERVER_MEDDLE_DBNAME}" -h "${DBSERVER_IP}" -e "${query}"
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to create an auth entry in UserAuthMap Table"
        return 1
    fi

    # Show the current entries in the DB
    echo "Database Entries:"
    query="select userID, userName, blockTrackers, monitorSSL from UserConfigs where userName='${clientName}';"    
    mysql -u "${DBSERVER_MEDDLE_USERNAME}" --password="${DBSERVER_MEDDLE_PASSWORD}" -D "${DBSERVER_MEDDLE_DBNAME}" -h "${DBSERVER_IP}" -e "${query}"
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to view entries created"
        return 1
    fi

    query="select userID, authToken from UserAuthMap where userID IN (SELECT userID from UserConfigs where userName='${clientName}');"
    mysql -u "${DBSERVER_MEDDLE_USERNAME}" --password="${DBSERVER_MEDDLE_PASSWORD}" -D "${DBSERVER_MEDDLE_DBNAME}" -h "${DBSERVER_IP}" -e "${query}"
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to view entries created"
        return 1
    fi
    return 0
}

function reloadVPNCredentials {
    # Add an entry for XAUTH required for iOS users in the ipsec.secrets file
    echo "${clientName} : XAUTH \"${clientPassword}\"" >> ${VPN_ROOT}/fs/etc/ipsec.secrets
    res=`cat ${VPN_ROOT}/fs/etc/ipsec.secrets | grep ${clientName}`
    if [ "${res}" == "" ];
    then
        echo "ERROR: Unable to find credentials for ${clientName} in ipsec.secrets file"
        return 1
    fi

    ${VPN_ROOT}/module reloadconf
    if [ $? -ne 1 ]
    then
        echo "ERROR: Unable to reload the VPN configurations. Please manually reload the configurations"
        echo "Steps to manually reload the confs"
        echo "Teleport to the VPN app"
        echo "Execute /usr/sbin/ipsec rereadall"
        echo "Check if credentials have been loaded in the charon log present in /opt/vpn/logs/"
        return 1
    fi
    return 0
}


######################################################################
# The script executes the following functions ########################
######################################################################
performChecks
if [ $? -ne 0 ];
then 
    exit 1
fi

autoCred=0
clientName=""
clientPassword=""
parse_args $@
if [ ${autoCred} -eq 1 ];
then 
    echo "Automatically assigning credentials"
    createCredentials
fi

if [ "${clientName}" == "" ] || [ "${clientPassword}" == "" ];
then
    echo "ERROR: Invalid userName: '${clientName}' or invalid password : '${clientPassword}'"
    usage $0
    exit 1
fi

echo "Creating an account with the following credentials"
echo "userName: ${clientName}"
echo "password: ${clientPassword}"
setVariables

createP12File
if [ $? -ne 0 ]
then
    echo "ERROR: Unable to create the .p12 file"
    exit 1
fi

generateMobileConfig
if [ $? -ne 0 ]
then
    echo "ERROR: Unable to create the .mobileconfig file required for iOS devices"
    exit 1
fi

createDBEntries
if [ $? -ne 0 ]
then
    echo "ERROR: Unable to create DB entries"
    exit 1
fi

reloadVPNCredentials
if [ $? -ne 0 ]
then
    echo "ERROR: Unable to create DB entries"
    exit 1
fi
echo "Success in creating the following account:"
echo "userName : ${clientName}"
echo "password : ${clientPassword}"

