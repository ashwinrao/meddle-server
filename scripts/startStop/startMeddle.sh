#!/bin/bash

# First load the conf files. Note some variables that depend on the containers to be running will be stale.
set -a 
source ./meddle.config > /dev/null 2>&1

#TODO:: Uncomment this line if you do not wish to see every command executed
#set -x

function setupOVS {    
    # This is needed to ensure that the network manager does not try to bring up random interfaces 
    service network-manager stop > /dev/null 2>&1
    service strongswan stop > /dev/null 2>&1

    ip addr del ${MEDDLE_GATEWAY_IP}/${MEDDLE_GATEWAY_PREFIX} dev ${MEDDLE_DOCKER_INTERFACE}
    if [ $? -ne 0 ];
    then 
        echo "ERROR: Unable to remove the IP assignment done to the docker interface ${MEDDLE_DOCKER_INTERFACE}"
        return 1
    fi    

    # Compute the broadcast IP used
    bcastip=$(ipcalc ${MEDDLE_GATEWAY_IP}/${MEDDLE_GATEWAY_PREFIX} | grep "Broadcast" | awk '{print $2}')
    if [ "${bcastip}" == "" ]
    then 
        echo "ERROR: Unable to obtain broadcast IP address for ${ipaddr}/${prefix}"
        return 1
    fi

    # Create an ovs switch in the namespace created for the switch 
    ovs-vsctl add-br ${MEDDLE_VSWITCH_INTERFACE}
    if [ $? -ne 0 ];
    then 
        echo "ERROR: Unable to create a vswitch named ${MEDDLE_VSWITCH_INTERFACE}"
        echo "ERROR: Check if meddle was shutdown correctly. Try manually removing the switch using the command ovs-vsctl del-br ${MEDDLE_VSWITCH_INTERFACE} and restart docker."
        return 1
    fi    

    # Assign the gateway IP to the interface created
    ip addr add ${MEDDLE_GATEWAY_IP}/${MEDDLE_GATEWAY_PREFIX} broadcast ${bcastip} dev ${MEDDLE_VSWITCH_INTERFACE}
    if [ $? -ne 0 ];
    then 
        echo "ERROR: Unable to assign ${ipaddr}/${prefix} to virtual interface ${hostovs}"
        return 1
    fi
     
    ovs-ofctl del-flows ${MEDDLE_VSWITCH_INTERFACE}
    if [ $? -ne 0 ];
    then 
        echo "ERROR: Unable to remove the flow table entries in ${MEDDLE_VSWITCH_INTERFACE}"
        return 1
    fi    
 
    ovs-ofctl add-flow ${MEDDLE_VSWITCH_INTERFACE} priority=1,actions=normal
    if [ $? -ne 0 ];
    then 
        echo "ERROR: Unable to add default forwarding rule in ${MEDDLE_VSWITCH_INTERFACE}"
        return 1
    fi

    return 0
}

function startMeddleModule {
    moduleName=$1
    moduleRoot=$2
    vethDev=$3

    # The path for the namespaces where the 
    netnsroot="/var/run/netns/"
    procns="/ns/net"
    # The name of the ethernet device used by the module.
    device="eth0"

    # Start the module
    ${moduleRoot}/module start
    if [ $? -ne 0 ]
    then
        echo "ERROR: Unable to start ${moduleName}"
        return 1
    fi
    #################################### 
    # Steps to assign the name of the module to its network namespace
    # Get the pid for the module. For better access, the network namespace of this pid is renamed to that of the modulelication. 
    modulePid=$(docker inspect -f '{{.State.Pid}}' ${moduleName})
    if [ $? -ne 0 ]
    then 
        echo "ERROR: Unable to get the PID for the module ${moduleName}"
        return 1
    fi
    
    mkdir -p ${netnsroot}
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to create the root path for netns: ${netnsroot}"
        return 1
    fi

    # Check if the network namespace exists 
    if [ -e "${netnsroot}/${moduleName}" ];
    then
        echo "ERROR: A network namespace of the name ${moduleName} already exists"
        return 1
    fi

    #TODO:: Check using ip netns as well 
    # Rename the network namespace with the module name
    ln -sf /proc/${modulePid}/${procns} ${netnsroot}/${moduleName}
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to rename the container's network namespace as ${container}"
    fi
    sync

    # Check if the namespace exists, grep -x searches for exact match
    contret=`ip netns list | grep -x ${moduleName}`
    if [ ! -e ${netnsroot}/${container} ] || [ "${contret}" == "" ] ;
    then
        echo "ERROR: Unable to find find the newly created network namespace of name ${moduleName}"
        return 1
    fi

    echo "Successfuly assigned the name of the meddlemodule ${moduleName} to its network namespace." 
    #################################### 
    # Steps to plug the container to the vswitch 
    # Find its peer.
    peerid=$(ip netns exec ${moduleName} ethtool -S ${device} | grep "peer" | awk '{print $2}')
    if [ $? -ne 0 ] || [ ${peerid} -lt 0 ] ;
    then
        echo "ERROR: Unable to find the interface index of ${device} in ${moduleName}";
        return 1
    fi 

    peername=$(ip link list | grep "^${peerid}" | awk '{print $2}' | cut -d ':' -f 1 | tr -d ' ')
    if [ $? -ne 0 ] || [ ${peername} == "" ]
    then
        echo "ERROR: Unable to find the peer of the ${device} device used by ${moduleName}";
        return 1
    fi
 
    # Unplug the interace from the docker bridge
    brctl delif ${MEDDLE_DOCKER_INTERFACE} ${peername}
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to detach ${peername} from bridge ${MEDDLE_DOCKER_INTERFACE}"
        return 1
    fi
 
    # This renaming of the virtual interfaces is done to ensure consistency in the names of the interfaces 
    ip link set dev ${peername} down
    if [ $? -ne 0 ];
    then
         echo "ERROR: Unable to bring ${peername} before renaming it"        
         exit 1
    fi

    # The length of the name is limited! Fix the length limits, or use a variable for it. 
    ip link set dev ${peername} name ${vethDev}
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to rename ${peername} to ${vethDev}"
        return 1
    fi

    ip link set dev ${vethDev} up
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to bring ${vethDev} up"
        return 1
    fi
    
    ovs-vsctl add-port ${MEDDLE_VSWITCH_INTERFACE} ${vethDev}
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to plug ${vethDev} to ${switch}"
        return 1
    fi
    echo "Successfully plugged the meddlemodule ${moduleName} to the switch ${MEDDLE_VSWITCH_INTERFACE}"

    return 0
}

function startPortMirroring {
    echo "Executing the steps required to enable port mirroring"
    ip link add ${PKTMONITOR_TAP_DEVICE} type veth peer name ${PKTMONITOR_VSTAP_DEVICE}
    if [ $? -ne 0 ];
    then
         echo  "ERROR: Unable to create the virtual interfaces required for packet monitoring"
         return 1
    fi

    ovs-vsctl add-port ${MEDDLE_VSWITCH_INTERFACE} ${PKTMONITOR_VSTAP_DEVICE} 
    if [ $? -ne 0 ];
    then
         echo  "ERROR: Unable to plug the ${PKTMONITOR_VSTAP_DEVICE} virtual interface  to the ${MEDDLE_VSWITCH_INTERFACE} vswitch"
         return 1
    fi

    ovs-vsctl \
          -- --id=@m create mirror name=pmonmirror \
          -- add bridge ${MEDDLE_VSWITCH_INTERFACE} mirrors @m \
          -- --id=@pvpn get port ${VPN_VETH_DEVICE} \
          -- set mirror pmonmirror select_src_port=@pvpn select_dst_port=@pvpn \
          -- --id=@ppmon get port ${PKTMONITOR_VSTAP_DEVICE} \
          -- set mirror pmonmirror output-port=@ppmon

    if [ $? -ne 0 ];
    then
         echo  "ERROR: Unable to configure the mirroring rule"
         return 1
    fi

    ip link set ${PKTMONITOR_TAP_DEVICE} netns ${PKTMONITOR_HOSTNAME}    
    if [ $? -ne 0 ];
    then
         echo  "ERROR: Unable to move the ${PKTMONITOR_TAP_DEVICE} tap interface to the ${PKTMONITOR_HOSTNAME} network namespace"
         return 1
    fi    

    ip netns exec ${PKTMONITOR_HOSTNAME} ip link set dev ${PKTMONITOR_TAP_DEVICE} up 
    if [ $? -ne 0 ];
    then
         echo  "ERROR: Unable to bring the ${PKTMONITOR_TAP_DEVICE} tap interface up"
         return 1
    fi    
 
    ip link set dev ${PKTMONITOR_VSTAP_DEVICE} up 
    if [ $? -ne 0 ];
    then
         echo  "ERROR: Unable to bring the ${PKTMONITOR_VSTAP_DEVICE} interface up"
         return 1
    fi    
    echo "Success: Configured port mirroring"
    echo "Success: All traffic traversing ${VPN_VETH_DEVICE} will be visible on ${PKTMONITOR_VETH_DEVICE}"    
    return 0
#ovs-vsctl \
# -- --id=@m create mirror name=tcpdumpmirror \
# -- add bridge ${vswitchname} mirrors @m \
# -- --id=@pingress get port vethingress \
# -- set mirror tcpdumpmirror select_src_port=@pingress select_dst_port=@pingress \
# -- --id=@vtcpdump get port vethtcpdump \
# -- set mirror tcpdumpmirror output-port=@vtcpdump
#ovs-vsctl list Bridge ${vswitchname}
}


function updateSDNController {      
     # ovs-vsctl set-controller ${MEDDLE_VSWITCH_INTERFACE} tcp:${ONOS_IP}:6633 
     # http://openvswitch.org/pipermail/discuss/2012-January/006276.html and http://openvswitch.org/pipermail/discuss/2012-January/006272.html
     ovs-vsctl set-controller ${MEDDLE_VSWITCH_INTERFACE} ptcp: 
     if [ $? -ne 0 ];
     then
        echo "ERROR: Unable to set controller for ${MEDDLE_VSWITCH_INTERFACE} to passively receive commands over tcp"
        return 1
     fi 
     echo "Successfully set controller for ${MEDDLE_VSWITCH_INTERFACE} to passively receive commands over tcp"
     return 0
}


function mainFunc() {
    setupOVS
    if [ $? -ne 0 ]
    then 
        echo "ERROR: Unable to start ovs"
        return 1
    fi

    startMeddleModule ${ORCHESTRATOR_HOSTNAME} ${ORCHESTRATOR_ROOT} ${ORCHESTRATOR_VETH_DEVICE} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${ORCHESTRATOR_HOSTNAME}"
        return 1
    fi

    # This is required to reload the variables of the orchestrator
    source ./meddle.config > /dev/null 2>&1

    startMeddleModule ${DBSERVER_HOSTNAME} ${DBSERVER_ROOT} ${DBSERVER_VETH_DEVICE} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${DBSERVER_HOSTNAME}"
        return 1
    fi

    startMeddleModule ${PKTMONITOR_HOSTNAME} ${PKTMONITOR_ROOT} ${PKTMONITOR_VETH_DEVICE} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${PKTMONITOR_HOSTNAME}"
        return 1
    fi

    startMeddleModule ${VPN_HOSTNAME} ${VPN_ROOT} ${VPN_VETH_DEVICE} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${VPN_HOSTNAME}"
        return 1
    fi


    startMeddleModule ${TRACKERFILTER_HOSTNAME} ${TRACKERFILTER_ROOT} ${TRACKERFILTER_VETH_DEVICE} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${TRACKERFILTER_HOSTNAME}"
        return 1
    fi

    startMeddleModule ${SSLMONITOR_HOSTNAME} ${SSLMONITOR_ROOT} ${SSLMONITOR_VETH_DEVICE} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${SSLMONITOR_HOSTNAME}"
        return 1
    fi


    # Now that the containers have been spawned, update the hosts file in the orchestrator to allow containers to communicate using the hostnames.
    ${ORCHESTRATOR_ROOT}/module updatehosts
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error updating the hosts file in the ${ORCHESTRATOR_HOSTNAME}"
        return 1
    fi


    startPortMirroring
    if [ $? -ne 0 ]
    then 
        echo "ERROR: Unable to mirror ports"
        return 1
    fi

    # This is required to reload the variables of the onos
    source ./meddle.config > /dev/null 2>&1

    updateSDNController
    if [ $? -ne 0 ]
    then
         echo "ERROR: Unable to specify SDN Controller"
         return 1 
    fi 
    echo "Success: Started meddle"
}

function firewallVpnTraffic {
    fwAction=$1
    iptables ${fwAction} INPUT -p udp -m multiport --destination-ports 50,51,500,4500 -j DROP
    if [ $? -ne 0 ]
    then 
        echo "ERROR: Unable to perform action ${fwAction} on VPN ports"
        return 1
    fi
    #iptables --list -n
    echo "Success: ${fwAction}-(e)d rule to DROP traffic on VPN ports"
}

function ehighlight {
   echo "ERROR:*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*!*"
}

if [[ $EUID -ne 0 ]]; then
   ehighlight 
   echo "ERROR: This script must be run as root" 1>&2
   ehighlight 
   exit 1
fi


# First stop the VPN traffic to ensure that VPN tunnels are created only after packet monitoring has been started
firewallVpnTraffic "--append"
if [ $? -ne 0 ]
then
    ehighlight 
    echo "ERROR: Unable to setup the firewall rules required before starting meddle"
    ehighlight 
    exit 1
fi 

# Start the Meddle modules
retCode=0
mainFunc 
if [ $? -ne 0 ]
then
    ehighlight 
    echo "ERROR: Unable to start Meddle"
    ehighlight 
    retCode=1
fi 

# Allow the VPN traffic
firewallVpnTraffic "--delete"
if [ $? -ne 0 ]
then
    ehighlight 
    echo "Error: Unable to remove the firewall rules invoked before starting meddle. YOU NEED TO MANUALLY REMOVE THESE RULES."
    ehighlight 
    retCode=1
fi
exit ${retCode}

