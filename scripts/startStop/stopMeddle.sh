#!/bin/bash

# First load the conf files. Note some variables that depend on the containers to be running will be stale.
set -a 
source ./meddle.config > /dev/null 2>&1

#TODO:: Uncomment this line if you do not wish to see every command executed
#set -x
function stopMeddleModule {
    moduleName=$1
    moduleRoot=$2
    vethDev=$3

    # The path for the namespaces where the 
    netnsroot="/var/run/netns/"
    procns="/ns/net"
    # The name of the ethernet device used by the module.
    device="eth0"

    ${moduleRoot}/module stop
    if [ $? -ne 0 ]
    then
        echo "ERROR: Unable to stop ${moduleName}"
        return 1
    fi
 
    ip netns del ${moduleName}
    # Check if the network namespace exists 
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to remove namespace of name ${moduleName}"
        return 1
    fi

    echo "Stopped the module ${moduleName}"
    return 0
}

function stopOVS {

    ovs-vsctl del-br ${MEDDLE_VSWITCH_INTERFACE}
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to remove a vswitch named ${MEDDLE_VSWITCH_INTERFACE}"
        return 1
    fi

    ip link del ${MEDDLE_DOCKER_INTERFACE}
    if [ $? -ne 0 ];
    then
        echo "ERROR: Unable to remove a docker interface ${MEDDLE_DOCKER_INTERFACE}"
        return 1
    fi

    service docker stop
    service docker start 
    echo "Done Stopping OVS"
# ovs-vsctl destroy Mirror pmonmirror -- clear Bridge vsbr0 mirrors
}

function stopMeddle {

    stopMeddleModule ${VPN_HOSTNAME} ${VPN_ROOT} ${VPN_VETH_DEVICE}
    if [ $? -ne 0 ]
    then
        echo "ERROR: Error stopping the ${VPN_HOSTNAME}"
        return 1
    fi

    stopMeddleModule ${PKTMONITOR_HOSTNAME} ${PKTMONITOR_ROOT} ${PKTMONITOR_VETH_DEVICE}
    if [ $? -ne 0 ]
    then
        echo "ERROR: Error stopping the ${PKTMONITOR_HOSTNAME}"
        return 1
    fi

    stopMeddleModule ${DBSERVER_HOSTNAME} ${DBSERVER_ROOT} ${DBSERVER_VETH_DEVICE}
    if [ $? -ne 0 ]
    then
        echo "ERROR: Error stopping the ${VPN_HOSTNAME}"
        return 1
    fi

    stopMeddleModule ${TRACKERFILTER_HOSTNAME} ${TRACKERFILTER_ROOT} ${TRACKERFILTER_VETH_DEVICE}
    if [ $? -ne 0 ]
    then
        echo "ERROR: Error stopping the ${TRACKERFILTER_HOSTNAME}"
        return 1
    fi

    stopMeddleModule ${SSLMONITOR_HOSTNAME} ${SSLMONITOR_ROOT} ${SSLMONITOR_VETH_DEVICE}
    if [ $? -ne 0 ]
    then
        echo "ERROR: Error stopping the ${SSLMONITOR_HOSTNAME}"
        return 1
    fi

    stopMeddleModule ${ORCHESTRATOR_HOSTNAME} ${ORCHESTRATOR_ROOT} ${ORCHESTRATOR_VETH_DEVICE}
    if [ $? -ne 0 ]
    then
        echo "ERROR: Error stopping the ${ORCHESTRATOR_HOSTNAME}"
        return 1
    fi

    stopOVS
    if [ $? -ne 0 ]
    then
        echo "ERROR: Unable to stop OVS"
        return 1
    fi
    echo "Success: Stopped Meddle"
}

if [[ $EUID -ne 0 ]]; then
   echo "ERROR: This script must be run as root" 1>&2
   exit 1
fi

stopMeddle
if [ $? -ne 0 ]
then
    echo "ERROR: Unable to stop Meddle. YOU NEED TO MANUALLY EXECUTE THE STEPS TO STOP MEDDLE"
    exit 1
fi




