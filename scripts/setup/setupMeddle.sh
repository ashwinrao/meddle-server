#!/bin/bash

# First load the conf files. Note some variables that depend on the containers to be running will be stale.
set -a 
source ./meddle.config > /dev/null 2>&1

#TODO:: Uncomment this line if you do not wish to see every command executed
#set -x

function setupMeddleModule {
    moduleName=$1
    moduleRoot=$2
    moduleImage=$3

    echo "*** Setting up ${moduleName} ***"    
    ${moduleRoot}/module forcesetup
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error setting up ${moduleName}"
        return 1
    fi
    echo "***********************************************************"
    echo "*** SUCCESS in setting up ${moduleName} ***"
    echo "***********************************************************"
    return 0
}

function installPackages {
    # Setups required for DOCKER SETUP
    echo "************************************************************************"
    echo "Installing the latest version of Docker"
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
    echo deb https://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list
    apt-get update
    apt-get install -y lxc-docker
    sleep 3
    echo "Starting Docker service"
    service docker restart 
    
    # Install the required packages
    echo "************************************************************************"
    echo "Installing the packages required to manage the meddle modules"
    apt-get install -y bridge-utils
    apt-get install -y openvswitch-switch
    apt-get install -y iptables ipcalc
    apt-get install -y strongswan
    apt-get install -y mysql-client
  

    echo "************************************************************************"
    echo "Disabling the default firewall"   
    ufw disable

    echo "************************************************************************"
    echo "Disabiling the auto start of some services"
    echo "manual" | tee /etc/init/strongswan.override    
    service strongswan stop    
}
 
function mainFunc() {  
    echo "************************************************************************"
    echo "Setup will remove the existing installation!!. Do you wish to continue? "
    echo -en "Press y to continue: "
    read yn >/dev/null 2>&1 3>&1
    case $yn in
        [Yy]* ) shift;;
            * ) shift; exit 0;
    esac
 
    installPackages 

    cd certificates
    ./setupCertificates.sh
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error setting up certificates"
        return 1
    fi
    cd -

    # Setup the Meddle Modules
    echo "************************************************************************"
    echo "Now setting up the Meddle modules" 
    setupMeddleModule ${DBSERVER_HOSTNAME} ${DBSERVER_ROOT} ${DBSERVER_IMAGENAME}
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${DBSERVER_HOSTNAME}"
        return 1
    fi

    setupMeddleModule ${ORCHESTRATOR_HOSTNAME} ${ORCHESTRATOR_ROOT} ${ORCHESTRATOR_IMAGENAME}
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${DBSERVER_HOSTNAME}"
        return 1
    fi

    setupMeddleModule ${PKTMONITOR_HOSTNAME} ${PKTMONITOR_ROOT} ${PKTMONITOR_IMAGENAME} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${PKTMONITOR_HOSTNAME}"
        return 1
    fi

    setupMeddleModule ${VPN_HOSTNAME} ${VPN_ROOT} ${VPN_IMAGENAME} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${VPN_HOSTNAME}"
        return 1
    fi

    setupMeddleModule ${TRACKERFILTER_HOSTNAME} ${TRACKERFILTER_ROOT} ${TRACKERFILTER_IMAGENAME} 
    if [ $? -ne 0 ] 
    then
        echo "ERROR: Error starting the ${TRACKERFILTER_HOSTNAME}"
        return 1
    fi   

    setupMeddleModule ${SSLMONITOR_HOSTNAME} ${SSLMONITOR_ROOT} ${SSLMONITOR_IMAGENAME}
    if [ $? -ne 0 ]
    then
        echo "ERROR: Error starting the ${SSLMONITOR_HOSTNAME}"
        return 1
    fi
}

if [[ $EUID -ne 0 ]]; then
   echo "ERROR: This script must be run as root" 1>&2
   exit 1
fi


mainFunc 
if [ $? -ne 0 ]
then
    echo "Error: Unable to setup Meddle"
    exit 1
fi 

exit 0
