#!/bin/bash
#set -x
set -a
source ./meddle.config > /dev/null 2>&1

ipsecBin=`which ipsec`
echo "########################################"
echo "Generating server certificates in ${PWD}"
echo "########################################"


if [ "${ipsecBin}" == "" ] 
then
   echo "Error: Unable to locate the ipsec binary of strongswan."
   echo "Please install strongswan using the following command on ubuntu/debian systems: sudo apt-get install strongswan"
   exit 1
fi 

${ipsecBin} pki --gen --outform pem > ${VPN_SERVER_PRIVKEY}
# Sign a certificate using this key 

echo "Creating the certificate ${serverCertName}"
${ipsecBin} pki --pub --in ${VPN_SERVER_PRIVKEY} | ${ipsecBin} pki --issue --cacert ${VPN_CA_CERTIFICATE} --cakey ${VPN_CA_PRIVKEY} --dn "C=${VPN_CA_COUNTRY}, O=${VPN_CA_ORGANIZATION}, CN=${MEDDLE_HOSTNAME}" --san="${MEDDLE_HOSTNAME}" --flag serverAuth --flag ikeIntermediate --outform pem > ${VPN_SERVER_CERTIFICATE}
# Voila! Now you have a valid certificate

echo "The Private key is present in ${PWD}/${VPN_SERVER_PRIVKEY}"
echo "The Certificate is present in ${PWD}/${VPN_SERVER_CERTIFICATE}"
chmod 644 ${VPN_SERVER_PRIVKEY}
chmod 644 ${VPN_SERVER_CERTIFICATE}
exit 0
