#!/bin/bash

#set -x
set -a
source ./meddle.config > /dev/null 2>&1

if [[ $EUID -ne 0 ]]; then
   echo "ERROR: This script must be run as root" 1>&2
   exit 1
fi

# Define the path to store the certificates
vpnCertRoot=${VPN_ROOT}/fs/etc/ipsec.d/
sslmonitorCertRoot=${SSLMONITOR_ROOT}/fs/opt/sslmonitor/certs/

# timestamp for backups
timestamp=`date '+%s'`

#####################################################################
## FOR VPN 
#####################################################################

certRoot=${vpnCertRoot}
mkdir -p ${certRoot}/cacerts/
mkdir -p ${certRoot}/certs/
mkdir -p ${certRoot}/private/

#backup
cp -vf ${certRoot}/cacerts/${VPN_CA_CERTIFICATE} \
   ${certRoot}/cacerts/${VPN_CA_CERTIFICATE}.${timestamp}

#copy
cp -vf ${VPN_CA_CERTIFICATE} \
   ${certRoot}/cacerts/

#backup
cp -vf ${certRoot}/certs/${VPN_SERVER_CERTIFICATE} \
   ${certRoot}/certs/${VPN_SERVER_CERTIFICATE}.${timestamp} 

#copy
cp -vf ${VPN_SERVER_CERTIFICATE} \
   ${certRoot}/certs/

#backup
cp -vf ${certRoot}/private/${VPN_SERVER_PRIVKEY} \
   ${certRoot}/private/${VPN_SERVER_PRIVKEY}.${timestamp} 

cp -vf ${VPN_SERVER_PRIVKEY} \
   ${certRoot}/private/


#####################################################################
## FOR SSLMONITOR
#####################################################################

certRoot=${sslmonitorCertRoot}

mkdir -p ${certRoot}

# backup
cp -vf ${certRoot}/${VPN_CA_CERTIFICATE} \
       ${certRoot}/${VPN_CA_CERTIFICATE}.${timestamp}

#copy
cp -vf ${VPN_CA_CERTIFICATE} \
       ${certRoot}/

#backup
cp -vf ${certRoot}/${VPN_CA_PRIVKEY} \
       ${certRoot}/${VPN_CA_PRIVKEY}.${timestamp} 

# copy 
cp -vf ${VPN_CA_PRIVKEY} \
       ${certRoot}/

echo "Copied the certificates"
exit 0
