#!/bin/bash


mainFunc() {
    echo "Creating the CA certificates"
    ./genCACert.sh
    if [ $? -ne 0 ];
    then
        echo "Error: Unable to create CA certificates"
        return 1
    fi

    echo "Creating the Server Certificates"
    ./genServerCert.sh
    if [ $? -ne 0 ];
    then
        echo "Error: Unable to create Server certificates"
        return 1
    fi
 

    echo "Installing the certificates"
    ./installCertificates.sh
    if [ $? -ne 0 ];
    then
        echo "Error: Unable to install the certificates"
        return 1
    fi 
}

if [[ $EUID -ne 0 ]]; then
   echo "ERROR: This script must be run as root" 1>&2
   exit 1
fi


mainFunc
if [ $? -ne 0 ]
then
    echo "Error: Unable to setup certificates"
    exit 1
fi

exit 0

