#!/bin/bash

echo "########################################"
echo "Generating the CA certificates in ${PWD}"
echo "########################################"
#set -x
set -a
source ./meddle.config > /dev/null 2>&1

ipsecBin=`which ipsec`


if [ "${ipsecBin}" == "" ] 
then
   echo "Error: Unable to locate the ipsec binary of strongswan."
   echo "Please install strongswan using the following command on ubuntu/debian systems: sudo apt-get install strongswan"
   exit 1
fi 

${ipsecBin} pki --gen --outform pem > ${VPN_CA_PRIVKEY}
# Sign a certificate using this key 
${ipsecBin} pki --self --in ${VPN_CA_PRIVKEY} --dn "C=${VPN_CA_COUNTRY}, O=${VPN_CA_ORGANIZATION}, CN=${VPN_CA_NAME}" --ca --outform pem > ${VPN_CA_CERTIFICATE}
# Voila! Now you are a CA
chmod 644 ${VPN_CA_PRIVKEY}
chmod 644 ${VPN_CA_CERTIFICATE}
echo "The Private key is present in ${PWD}/${VPN_CA_PRIVKEY}"
echo "The Certificate is present in ${PWD}/${VPN_CA_CERTIFICATE}"
exit 0

