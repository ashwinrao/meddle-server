#!/bin/bash

echo  "Creating meddle-server.tar.bz2"

rm -f meddle-server.tar.bz2
mkdir -p /tmp/meddle-server-build/ 
cp -vRf ../../../meddle-server/ /tmp/meddle-server-build/
rm -Rf /tmp/meddle-server-build/meddle-server/.git
cd /tmp/meddle-server-build
tar -acvf meddle-server.tar.bz2 meddle-server/
cd -
mv /tmp/meddle-server-build/meddle-server.tar.bz2 . 
rm -Rf /tmp/meddle-server-build

