1 Directory Structure
=======================

The following files/folders are present in this folder
~~~
README     - this file
modules    - the folder which contains the scripts and data for each meddle module
traces     - the folder which contains the symbolic links to the traffic traces captured by the meddle modules
~~~


###########################################################
# Steps to build Meddle
###########################################################

The steps have been written for Ubuntu 14.04 the LTS version of Ubuntu. 

1 Copy the Meddle server package in \/opt\/meddle-server\/ as root and change the owner of all the files in /opt/meddle-server to root.
~~~
$> sudo su 
$> cp meddle-server.tgz /opt/
$> cd /opt/
$> tar -axvf meddle-server.tar.bz2
$> chown -R root.root *
~~~

2 Update the meddle.config file present in /opt/meddle-server/modules/meddle.config. Make sure that the following variables are correct. /Note that you that all the variables are within a quote/
~~~
MEDDLE\_ROOT      points to the folder in which meddle is installed (/opt/meddle-server/)
MEDDLE\_HOSTNAME  is the hostname used by the mobile devices to contact the Meddle server. 
~~~

3 Setup the meddle services using the setupApps.sh in /opt/meddle-server/scripts/setup. /This step will take time (at least 5 minutes) and depends on the Internet connectivity./
~~~
$> /opt/meddle-server/scripts/setup
$> ./setupMeddle.sh
Setup will remove the existing installation!!. Do you wish to continue? 
 Press y to continue: Y
...
~~~

###########################################################
# Steps to start Meddle
###########################################################

Execute the startMeddle.sh script in /opt/meddle-server/scripts/startStop folder

~~~
$> sudo su
$> cd /opt/meddle-server/scripts/startStop
$> ./startMeddle.sh
~~~

###########################################################
# Steps to stop Meddle
###########################################################

Execute the stopMeddle.sh script in /opt/meddle-server/scripts/startStop folder

~~~
$> sudo su
$> cd /opt/meddle-server/scripts/startStop
$> ./stopMeddle.sh
~~~





